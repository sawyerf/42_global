/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   back.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 16:01:42 by apeyret           #+#    #+#             */
/*   Updated: 2018/11/22 11:47:44 by glavigno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		is_putta(char **tab, t_piece *lst, int x, int y)
{
	int		i;
	int		len;
	t_coord	p;

	p = lst->crd;
	i = -1;
	len = ft_strlen(tab[0]);
	while (++i < EDGE)
	{
		if (x + p.x[i] >= len || y + p.y[i] >= len)
			return (0);
		if (tab[y + p.y[i]][x + p.x[i]] != EMPTY)
			return (0);
	}
	return (1);
}

void	put_piece(char **tab, t_piece *lst, int x, int y)
{
	int		i;
	t_coord	p;

	p = lst->crd;
	i = -1;
	while (++i < EDGE)
		tab[y + p.y[i]][x + p.x[i]] = lst->letter;
}

void	delete_piece(char **tab, t_piece *lst, int x, int y)
{
	int		i;
	t_coord	p;

	p = lst->crd;
	i = -1;
	while (++i < EDGE)
		tab[y + p.y[i]][x + p.x[i]] = EMPTY;
}

int		solve_grid(char **tab, t_piece *lst)
{
	int x;
	int y;

	x = 0;
	y = 0;
	if (!lst)
		return (1);
	while (tab[y])
	{
		x = 0;
		while (tab[y][x])
		{
			if (is_putta(tab, lst, x, y))
			{
				put_piece(tab, lst, x, y);
				if (solve_grid(tab, lst->next))
					return (1);
				delete_piece(tab, lst, x, y);
			}
			++x;
		}
		++y;
	}
	return (0);
}
