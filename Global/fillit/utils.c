/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glavigno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 17:42:20 by glavigno          #+#    #+#             */
/*   Updated: 2018/11/26 14:26:24 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

t_piece			*read_all(int fd)
{
	t_piece	*lst;
	char	str[BUFF_SIZE + 1];
	char	c;
	int		end;

	end = 0;
	c = 'A';
	lst = NULL;
	str[BUFF_SIZE - 1] = '\0';
	while ((read(fd, str, BUFF_SIZE)) > 0)
	{
		str[BUFF_SIZE] = '\0';
		if (c > 'Z')
			return (NULL);
		if (str[0] != '\n')
			lst = piece_add(lst, str, c++);
		else
			return (NULL);
		if (str[20] == '\0')
			end = 1;
		ft_strclr(str);
	}
	if (!end)
		return (NULL);
	return (lst);
}

t_piece			*find_minus(t_piece *lst)
{
	t_piece		*tmp;
	int			count;
	int			xmin;
	int			ymin;

	tmp = lst;
	while (tmp)
	{
		count = -1;
		xmin = EDGE;
		ymin = EDGE;
		while (++count < EDGE)
		{
			xmin = (tmp->crd.x[count] < xmin) ? tmp->crd.x[count] : xmin;
			ymin = (tmp->crd.y[count] < ymin) ? tmp->crd.y[count] : ymin;
		}
		count = -1;
		while (++count < EDGE)
		{
			tmp->crd.x[count] -= xmin;
			tmp->crd.y[count] -= ymin;
		}
		tmp = tmp->next;
	}
	return (lst);
}

t_piece			*piece_norm(t_piece *lst)
{
	t_piece		*tmp;
	int			x;
	int			y;
	int			count;

	tmp = lst;
	while (tmp)
	{
		x = -1;
		y = 0;
		count = 0;
		while (ft_runtab(tmp->line, &x, &y))
		{
			if ((tmp->line)[y][x] == BLOCK)
			{
				tmp->crd.x[count] = x;
				tmp->crd.y[count] = y;
				count++;
			}
		}
		tmp = tmp->next;
	}
	return (find_minus(lst));
}

t_piece			*piece_add(t_piece *lst, char *line, char c)
{
	t_piece		*tmp;
	t_piece		*i;

	i = lst;
	if (!(tmp = (t_piece*)malloc(sizeof(t_piece))))
		return (NULL);
	if (!lst)
		lst = tmp;
	else
	{
		while (i->next)
			i = i->next;
		i->next = tmp;
	}
	tmp->line = ft_strsplit(line, '\n');
	tmp->letter = c;
	tmp->next = NULL;
	return (lst);
}
