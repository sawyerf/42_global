/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glavigno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 17:35:19 by glavigno          #+#    #+#             */
/*   Updated: 2018/11/26 14:25:43 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int	check_size_nb(char **tab)
{
	int	count;
	int nb;
	int nbp;

	nbp = 0;
	nb = 0;
	count = 0;
	while (tab[count])
	{
		if (ft_strlen(tab[count]) != EDGE)
			return (0);
		nb += ft_strccount(tab[count], BLOCK);
		nbp += ft_strccount(tab[count], EMPTY);
		++count;
	}
	if (count == EDGE && nb == EDGE && nbp == 12)
		return (1);
	return (0);
}

int	check_input(char **tab)
{
	int		x;
	int		y;
	int		counter;

	y = 0;
	x = -1;
	counter = 0;
	while (ft_runtab(tab, &x, &y))
	{
		if (tab[y][x] == BLOCK)
		{
			if (y < (EDGE - 1) && tab[y + 1][x] == BLOCK)
				++counter;
			if (x < (EDGE - 1) && tab[y][x + 1] == BLOCK)
				++counter;
			if (y)
				if (tab[y - 1][x] == BLOCK)
					++counter;
			if (x)
				if (tab[y][x - 1] == BLOCK)
					++counter;
		}
	}
	return ((counter == 6 || counter == 8) ? 1 : 0);
}

int	check(t_piece *lst)
{
	if (!lst)
		return (0);
	while (lst)
	{
		if (!check_size_nb(lst->line) || !check_input(lst->line))
			return (0);
		lst = lst->next;
	}
	return (1);
}
