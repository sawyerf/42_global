/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glavigno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 17:37:21 by glavigno          #+#    #+#             */
/*   Updated: 2018/11/22 14:23:47 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include <sys/types.h>
# include <sys/uio.h>
# include <unistd.h>
# include <fcntl.h>
# include "libft/libft.h"

# define BLOCK '#'
# define EMPTY '.'
# define EDGE 4
# define BUFF_SIZE 21

typedef struct		s_coord
{
	int				x[4];
	int				y[4];
}					t_coord;

typedef struct		s_piece
{
	char			**line;
	char			letter;
	t_coord			crd;
	struct s_piece	*next;
}					t_piece;

t_piece				*piece_add(t_piece *lst, char *line, char c);
t_piece				*piece_norm(t_piece *lst);
t_piece				*read_all(int fd);
int					check(t_piece *lst);
int					get_next_line_ndl(const int fd, char **line);
int					run_tab(char **tab, int *x, int *y);
int					solve_grid(char **tab, t_piece *lst);
int					str_count(char *str, char c);
#endif
