/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: glavigno <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 17:38:27 by glavigno          #+#    #+#             */
/*   Updated: 2018/11/26 14:41:10 by glavigno         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	free_full_table(char **tab)
{
	char	**full;
	char	*tmp;

	full = tab;
	tmp = *tab;
	while (tmp)
	{
		ft_strdel(&tmp);
		tmp = *++tab;
	}
	free(full);
}

void	del_list(t_piece *lst)
{
	t_piece	*tmp;

	while (lst)
	{
		tmp = lst;
		free_full_table(tmp->line);
		lst = lst->next;
		free(tmp);
	}
}

char	**allocate_tab(char **tab, int size)
{
	char	**new_table;
	int		i;
	int		j;

	if (tab)
		free_full_table(tab);
	if (!(new_table = (char**)malloc(sizeof(char*) * (size + 1))))
		return (NULL);
	i = -1;
	while (++i < size)
		new_table[i] = ft_strnew(size);
	i = -1;
	while (++i < size)
	{
		j = 0;
		while (j < size)
			new_table[i][j++] = EMPTY;
		new_table[i][j] = '\0';
	}
	new_table[size] = NULL;
	return (new_table);
}

int		main(int ac, char **av)
{
	t_piece *lst;
	int		fd;
	char	**tab;
	int		size;

	tab = NULL;
	if (ac != 2)
		ft_exit("usage: fillit input_file", 1);
	if ((fd = open(av[1], O_RDONLY)) < 0)
		ft_exit("error", 1);
	lst = read_all(fd);
	if (!check(lst))
		ft_exit("error", 1);
	lst = piece_norm(lst);
	size = 2;
	if (!(tab = allocate_tab(tab, size)))
		ft_exit("error", 1);
	while (!solve_grid(tab, lst))
		tab = allocate_tab(tab, size++);
	ft_puttab(tab);
	free_full_table(tab);
	del_list(lst);
	return (0);
}
