/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 16:14:32 by apeyret           #+#    #+#             */
/*   Updated: 2018/11/14 17:03:24 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

int	get_next_line(int fd, char **line)
{
	char *buf;

	*line = NULL;
	buf = ft_strnew(BUFF_SIZE);
	while ((read(fd, buf, BUFF_SIZE)) > 0)
	{
		buf[BUFF_SIZE] = '\0';
		printf("%s", buf);
		ft_strclr(buf);
	}
	return (0);
}
