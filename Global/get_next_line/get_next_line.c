/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/12 15:45:19 by apeyret           #+#    #+#             */
/*   Updated: 2018/11/20 14:51:23 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static int	ft_strichr(char *s, char c, int n)
{
	int count;

	count = 0;
	while (s[count])
	{
		if (s[count] == c)
			break ;
		count++;
	}
	if (n && s[count] == c)
		return (count + 1);
	return (count);
}

static char	*ft_strnjoin(char **s1, char *s2, int n)
{
	char	*str;
	int		len;
	int		count;

	count = 0;
	if (!*s1 || !s2)
		return (NULL);
	len = ft_strlen(*s1);
	if (!(str = ft_strnew(len + n)))
		return (NULL);
	while ((*s1)[count])
	{
		str[count] = (*s1)[count];
		count++;
	}
	count = 0;
	while (s2[count] && count < n)
	{
		str[len + count] = s2[count];
		count++;
	}
	str[len + count] = '\0';
	ft_strdel(s1);
	return (str);
}

static int	ft_tmp(char **tmp, char **line, int *isr)
{
	if (!*tmp)
	{
		*tmp = ft_strnew(0);
		return (0);
	}
	if ((*tmp)[0] != '\0')
	{
		*isr = 1;
		*line = ft_strnjoin(line, *tmp, ft_strichr(*tmp, '\n', 0));
		if (ft_strchr(*tmp, 10))
		{
			*tmp = ft_strcpy(*tmp, *tmp + ft_strichr(*tmp, '\n', 1));
			return (1);
		}
		*tmp = ft_strcpy(*tmp, *tmp + ft_strichr(*tmp, '\n', 1));
	}
	return (0);
}

static int	ft_read(int fd, char **line, char **tmp, int *isr)
{
	int		ret;
	char	*buf;

	buf = ft_strnew(BUFF_SIZE);
	ret = read(fd, buf, BUFF_SIZE);
	buf[BUFF_SIZE] = '\0';
	if (ret == 0 && *isr == 1)
		return (2);
	else if (ret == 0 || ret == -1)
		return (ret);
	*isr = 1;
	*line = ft_strnjoin(line, buf, ft_strichr(buf, '\n', 0));
	if (ft_strchr(buf, 10))
	{
		*tmp = ft_strnjoin(tmp, buf + ft_strichr(buf, '\n', 1),
		ft_strlen(buf + ft_strichr(buf, '\n', 1)));
		return (2);
	}
	ft_strclr(buf);
	ft_strdel(&buf);
	return (1);
}

int			get_next_line(const int fd, char **line)
{
	static char	*tmp;
	char		buf[1];
	int			ret;
	int			isr;

	isr = 0;
	ret = 0;
	if (read(fd, buf, 0) == -1)
		return (-1);
	*line = ft_strnew(0);
	if (ft_tmp(&tmp, line, &isr))
		return (1);
	while ((ret = ft_read(fd, line, &tmp, &isr)) > 0)
		if (ret == 2)
			return (1);
	return (ret);
}
