/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/05 15:52:05 by apeyret           #+#    #+#             */
/*   Updated: 2018/11/05 18:09:39 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "main.h"

void		ft_putchar(char c)
{
	write(1, &c, 1);
}

void		ft_putstr(char *str)
{
	int		count;

	count = 0;
	while (str[count])
	{
		ft_putchar(str[count]);
		count++;
	}
}

int			file_read(char *name)
{
	int		file;
	int		ret;
	char	buf[1];

	file = open(name, O_RDONLY);
	if (file == -1)
		return (0);
	while ((ret = read(file, buf, 1)))
		ft_putchar(buf[0]);
	return (1);
}

int			main(int ac, char **av)
{
	if (ac == 1)
	{
		ft_putstr("File name missing.");
		return (0);
	}
	if (ac > 2)
	{
		ft_putstr("Too many arguments.");
		return (0);
	}
	return (file_read(av[1]));
}
