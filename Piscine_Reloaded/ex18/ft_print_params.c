/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_params.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/08 16:41:11 by apeyret           #+#    #+#             */
/*   Updated: 2018/08/09 11:08:50 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

int		main(int argc, char **argv)
{
	int	count;
	int counta;

	count = 1;
	counta = 0;
	while (count < argc)
	{
		while (argv[count][counta] != '\0')
		{
			ft_putchar(argv[count][counta]);
			counta++;
		}
		ft_putchar('\n');
		counta = 0;
		count++;
	}
}
