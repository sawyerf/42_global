/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_recursive_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/05 18:11:06 by apeyret           #+#    #+#             */
/*   Updated: 2018/11/05 18:11:09 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_bis(int rendu, int count)
{
	if (count < 0 || count >= 13)
		return (0);
	if (count == 0 || count == 1)
		return (1);
	return (count * ft_bis(rendu * count, count - 1));
}

int	ft_recursive_factorial(int nb)
{
	return (ft_bis(1, nb));
}
