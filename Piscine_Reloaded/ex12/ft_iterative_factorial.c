/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/05 18:10:36 by apeyret           #+#    #+#             */
/*   Updated: 2018/11/05 18:10:39 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_iterative_factorial(int nb)
{
	int rendu;
	int count;

	rendu = 1;
	count = 1;
	if (nb < 0 || nb >= 13)
		return (0);
	if (nb == 1 || nb == 0)
		return (1);
	while (count != nb + 1)
	{
		rendu = rendu * count;
		count++;
	}
	return (rendu);
}
