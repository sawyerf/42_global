/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_rev_params.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/08 20:07:36 by apeyret           #+#    #+#             */
/*   Updated: 2018/11/06 13:00:01 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_print(int argc, char **argv)
{
	int	count;
	int counta;

	count = 1;
	counta = 0;
	while (count < argc)
	{
		while (argv[count][counta] != '\0')
		{
			ft_putchar(argv[count][counta]);
			counta++;
		}
		ft_putchar('\n');
		counta = 0;
		count++;
	}
}

void	ft_swap(char **s1, char **s2)
{
	char *str;

	str = *s1;
	*s1 = *s2;
	*s2 = str;
}

int		ft_check(char **argv, int count, int diff)
{
	int counta;

	counta = 0;
	while (argv[count][counta] == argv[count + 1][counta] &&
		argv[count][counta] != '\0' && argv[count][counta] != '\0')
		counta++;
	if (argv[count][counta] > argv[count + 1][counta])
	{
		ft_swap(&argv[count], &argv[count + 1]);
		diff++;
	}
	return (diff);
}

void	ft_main(int argc, char **argv, int diff, int count)
{
	while (diff != 0)
	{
		if (count == argc - 1 && diff == 1)
		{
			diff = 0;
			count = 0;
		}
		if (count == argc - 1 && diff > 1)
		{
			diff = 1;
			count = 0;
		}
		if (argv[count][0] > argv[count + 1][0])
		{
			ft_swap(&argv[count], &argv[count + 1]);
			diff++;
		}
		if (argv[count][0] == argv[count + 1][0])
			diff = ft_check(argv, count, diff);
		count++;
	}
}

int		main(int argc, char **argv)
{
	int diff;
	int count;

	diff = 1;
	count = 0;
	if (argc == 1)
		return (0);
	ft_main(argc, argv, diff, count);
	ft_print(argc, argv);
}
