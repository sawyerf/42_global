/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sqrt.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/05 21:44:33 by apeyret           #+#    #+#             */
/*   Updated: 2018/08/07 11:46:18 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_sqrt(int nb)
{
	int count;
	int div;

	div = 1;
	count = 0;
	while (count * count < nb && count < div)
	{
		count++;
		div = nb / count;
	}
	if (count * count == nb)
		return (count);
	else
		return (0);
}
