/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strisdigit.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/26 18:04:03 by apeyret           #+#    #+#             */
/*   Updated: 2018/12/26 18:35:53 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strisdigit(char *s)
{
	int	count;

	count = 0;
	while (s[count])
	{
		if (!ft_isdigit(s[count]) && s[count] != '-' && s[count] != '+')
			return (0);
		count++;
	}
	return (1);
}
