# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: apeyret <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/11/08 14:35:55 by apeyret           #+#    #+#              #
#    Updated: 2019/02/22 17:18:09 by apeyret          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libft.a
CC = gcc
CFLAGS = -Wall -Wextra -Werror -I . -I ft_printf/
SRC = ft_atoi.c \
	ft_bzero.c \
	ft_isalnum.c \
	ft_isalpha.c \
	ft_isascii.c \
	ft_isdigit.c \
	ft_isprint.c \
	ft_itoa.c \
	ft_memcpy.c \
	ft_memccpy.c \
	ft_memalloc.c \
	ft_memchr.c \
	ft_memcmp.c \
	ft_memdel.c \
	ft_memmove.c \
	ft_memset.c \
	ft_putchar.c \
	ft_putchar_fd.c \
	ft_putendl.c \
	ft_putendl_fd.c \
	ft_putnbr.c \
	ft_putnbr_fd.c \
	ft_putstr.c \
	ft_putstr_fd.c \
	ft_strchr.c \
	ft_strclr.c \
	ft_strcpy.c \
	ft_strncpy.c \
	ft_strcat.c \
	ft_strncat.c \
	ft_strlcat.c \
	ft_strcmp.c \
	ft_strdel.c \
	ft_strdup.c \
	ft_strequ.c \
	ft_striter.c \
	ft_striteri.c \
	ft_strjoin.c \
	ft_strlen.c \
	ft_strncmp.c \
	ft_strnequ.c \
	ft_strnew.c \
	ft_strnstr.c \
	ft_strrchr.c \
	ft_strstr.c \
	ft_strsplit.c \
	ft_strtrim.c \
	ft_strsub.c \
	ft_tolower.c \
	ft_toupper.c \
	ft_exit.c \
	ft_puttab.c \
	ft_runtab.c \
	ft_strccount.c \
	ft_intlen.c \
	ft_strndup.c \
	ft_strlower.c \
	ft_cisin.c \
	ft_printf/ft_printf.c \
	ft_printf/pf_base.c \
	ft_printf/pf_ftoa.c \
	ft_printf/pf_len.c \
	ft_printf/pf_options.c \
	ft_printf/pf_parsing.c \
	ft_printf/pf_put.c \
	ft_printf/pf_router.c \
	ft_printf/pf_struct.c \
	ft_printf/pf_utils.c \
	ft_strichr.c \
	get_next_line.c \
	ft_strisdigit.c \
	ft_tabnew.c \
	ft_tablen.c \
	ft_tabdup.c \
	ft_preturn.c \
	ft_tabcpy.c \
	ft_tabdel.c

OBJ = $(SRC:.c=.o) 

all: $(NAME)

%.o: %.c
	@printf "\x1B[0;32m[Libft] Compilation [o.] \r"
	@$(CC) $(CFLAGS) -c $< -o $@
	@printf "\x1B[0;32m[Libft] Compilation [.o] \r"

$(NAME): $(OBJ)
	@ar rc $(NAME) $(OBJ)
	@printf "\x1B[0;32m[Libft] Compilation [OK]\n\x1B[0;0m"

clean:
	@/bin/rm -f $(OBJ)
	@printf "\033[0;31m[Libft] Deleted *.o\n\x1B[0;0m"

nofclean:
	@/bin/rm -f $(NAME)
	@printf "\033[0;31m[Libft] Deleted libft.a\n\x1B[0;0m"

fclean: nofclean clean

re: fclean all
