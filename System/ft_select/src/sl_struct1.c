/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sl_struct1.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/04 15:13:25 by apeyret           #+#    #+#             */
/*   Updated: 2019/03/04 15:15:49 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void	sl_chcfree(t_chc *chc)
{
	ft_strdel(&chc->s);
	free(chc);
	chc = NULL;
}

void	sl_chcsetone(t_chc *chc)
{
	int i;

	i = 0;
	while (chc)
	{
		chc->i = i;
		i++;
		chc = chc->onext;
	}
}

t_chc	*sl_chcdel(t_chc *chc)
{
	t_chc *tmp;

	tmp = chc->first;
	if (chc == tmp->first)
	{
		chc->next->oprev = NULL;
		while (tmp)
		{
			tmp->first = chc->next;
			tmp = tmp->onext;
		}
	}
	chc->prev->next = chc->next;
	chc->next->prev = chc->prev;
	if (chc->prev->onext)
		chc->prev->onext = chc->onext;
	if (chc->next->oprev)
		chc->next->oprev = chc->oprev;
	chc->next->curs = 1;
	tmp = chc->next->first;
	if (!chc->onext && !chc->oprev)
		tmp = NULL;
	sl_chcfree(chc);
	sl_chcsetone(tmp);
	return (tmp);
}

void	sl_chcdelall(t_chc **chc)
{
	t_chc	*tmp;

	while (*chc)
	{
		ft_strdel(&(*chc)->s);
		tmp = *chc;
		*chc = (*chc)->onext;
		free(tmp);
		tmp = NULL;
	}
}

t_chc	*sl_chcinit(char **av)
{
	int		count;
	t_chc	*chc;

	count = 1;
	chc = NULL;
	while (av[count])
	{
		chc = sl_chcadds(chc, av[count]);
		count++;
	}
	return (chc);
}
