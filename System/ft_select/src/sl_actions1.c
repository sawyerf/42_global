/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sl_actions1.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/04 14:18:30 by apeyret           #+#    #+#             */
/*   Updated: 2019/03/04 14:18:48 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void	sl_exit(t_opt opt)
{
	sl_clr();
	sl_chcdelall(&opt.chc);
	sl_termreset(opt);
	exit(0);
}

void	sl_selectall(t_chc *chc, int sl)
{
	while (chc)
	{
		chc->select = sl;
		chc = chc->onext;
	}
}
