/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sl_searchact.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/04 15:04:29 by apeyret           #+#    #+#             */
/*   Updated: 2019/03/04 15:06:35 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

int		sl_srcinit(t_src *src)
{
	src->str = NULL;
	if (!(src->str = ft_strnew(129)))
		return (1);
	src->str[0] = 0;
	src->allo = 128;
	src->size = 0;
	return (0);
}

void	sl_srcadd(t_src *src, char c)
{
	char	*s;

	if (src->size == src->allo)
	{
		if (!(s = ft_strnew(src->size + 129)))
			return ;
		src->allo += 128;
		ft_strcpy(s, src->str);
		ft_strdel(&src->str);
		src->str = s;
	}
	src->str[src->size] = c;
	src->size++;
	src->str[src->size] = 0;
}

void	sl_srcdel(t_src *src)
{
	if (src->size)
	{
		src->size--;
		src->str[src->size] = 0;
	}
}
