/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sl_signal.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/22 20:44:22 by apeyret           #+#    #+#             */
/*   Updated: 2019/03/04 14:37:35 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

extern t_opt g_opt;

void	sl_sigsize(int sig)
{
	(void)sig;
	sl_getline(&g_opt);
	sl_putchcall(g_opt);
	return ;
}

void	sl_sigquit(int sig)
{
	(void)sig;
	sl_clr();
	sl_chcdelall(&g_opt.chc);
	sl_termreset(g_opt);
	exit(0);
}

void	sl_sigstop(int sig)
{
	char	cp[2];

	(void)sig;
	sl_termreset(g_opt);
	cp[0] = g_opt.term.c_cc[VSUSP];
	cp[1] = 0;
	signal(SIGTSTP, SIG_DFL);
	ioctl(0, TIOCSTI, cp);
}

void	sl_sigfg(int sig)
{
	(void)sig;
	sl_terminit(NULL);
	signal(SIGTSTP, &sl_sigstop);
	sl_sigsize(0);
}

void	sl_sigset(void)
{
	signal(SIGWINCH, &sl_sigsize);
	signal(SIGCONT, &sl_sigfg);
	signal(SIGQUIT, &sl_sigquit);
	signal(SIGINT, &sl_sigquit);
	signal(SIGTSTP, &sl_sigstop);
}
