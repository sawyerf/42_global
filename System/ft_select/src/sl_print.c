/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sl_print.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/22 20:55:31 by apeyret           #+#    #+#             */
/*   Updated: 2019/03/04 16:31:34 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void	sl_putchc(t_opt opt, t_chc *chc)
{
	if (chc->select)
		ft_dprintf(opt.fd, "\e[7m", opt.chc->s);
	if (chc->src)
		ft_dprintf(opt.fd, "\x1B[33m");
	if (chc->curs)
		ft_dprintf(opt.fd, "\e[4m\e[31m");
	ft_dprintf(opt.fd, "[%-*s]\e[0m ", opt.lmax + 1, chc->s);
}

void	sl_putchcsrc(t_opt opt, t_src src)
{
	int		adv;
	int		fir;
	int		col;
	int		all;
	int		lenpr;

	col = opt.col / (opt.lmax + 4);
	all = (opt.line * col);
	fir = sl_getfirst(opt);
	adv = 0;
	if (all)
	{
		if (opt.len % all)
			lenpr = ft_dprintf(opt.fd, "[%d/%d]", (fir + all - (fir % all))
				/ all, ((opt.len + all - (opt.len % all)) / all));
		else
			lenpr = ft_dprintf(opt.fd, "[%d/%d]", (fir + all - (fir % all))
				/ all, opt.len / all);
	}
	if (!src.str)
		return ;
	if (src.size > opt.col - 1 - lenpr)
		adv = src.size - opt.col + 1 + lenpr;
	ft_dprintf(opt.fd, "/%s", src.str + adv);
}

t_chc	*sl_chctake(t_opt opt, t_chc *chc)
{
	int		stp;
	t_chc	*tmp;

	stp = sl_getfirst(opt);
	tmp = chc;
	while (chc && chc->i != stp)
		chc = chc->onext;
	return (chc);
}

void	sl_putline(t_chc *chc, t_opt opt)
{
	int		i;
	int		ii;
	t_chc	*tmp;

	i = 0;
	ii = 0;
	tmp = chc;
	while (tmp && ii < opt.col / (opt.lmax + 4))
	{
		if (!(i % opt.line))
		{
			sl_putchc(opt, tmp);
			ii++;
		}
		i++;
		tmp = tmp->onext;
	}
	ft_dprintf(opt.fd, "\n");
}

void	sl_putchcall(t_opt opt)
{
	t_chc	*chc;
	int		y;

	y = 0;
	sl_clr();
	if (opt.line < 1 || !(opt.col / (opt.lmax + 4)))
	{
		ft_dprintf(opt.fd, "Not enough space");
		return ;
	}
	chc = sl_chctake(opt, opt.chc);
	while (y < opt.line && chc)
	{
		sl_putline(chc, opt);
		chc = chc->onext;
		y++;
	}
	sl_putchcsrc(opt, opt.src);
}
