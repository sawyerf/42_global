/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sl_get.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/04 15:08:24 by apeyret           #+#    #+#             */
/*   Updated: 2019/03/04 18:19:02 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

int		sl_getfd(void)
{
	char	*s;
	int		fd;

	if (!isatty(0))
		return (-1);
	if (!(s = ttyname(0)))
		return (-1);
	if ((fd = open(s, O_WRONLY)) < 0)
		return (-1);
	return (fd);
}

int		sl_getfirst(t_opt opt)
{
	t_chc			*curs;
	int				col;
	int				maxp;

	col = opt.col / (opt.lmax + 4);
	maxp = col * opt.line;
	if (maxp >= opt.len)
		return (0);
	if (!opt.src.str)
		curs = sl_chccurs(opt.chc);
	else
	{
		if (!(curs = sl_chcfsrc(opt.chc)))
			curs = opt.chc;
	}
	if (!maxp)
		return (0);
	return (curs->i - (curs->i % maxp));
}

void	sl_getline(t_opt *opt)
{
	struct winsize	w;
	int				col;

	if (ioctl(0, TIOCGWINSZ, &w) < 0)
		exit(1);
	opt->col = w.ws_col;
	w.ws_row -= 1;
	if (w.ws_row <= 0 || !w.ws_col || w.ws_col < opt->lmax)
	{
		opt->line = 0;
		return ;
	}
	col = w.ws_col / (opt->lmax + 4);
	opt->line = w.ws_row;
}
