/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sl_actions.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/18 17:16:53 by apeyret           #+#    #+#             */
/*   Updated: 2019/03/04 14:30:59 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

t_chc	*sl_chccurs(t_chc *chc)
{
	while (!chc->curs)
		chc = chc->next;
	return (chc);
}

void	sl_return(t_opt opt)
{
	t_chc	*tmp;
	int		first;

	first = 0;
	sl_termreset(opt);
	tmp = opt.chc;
	while (opt.chc)
	{
		if (opt.chc->select)
		{
			if (!first)
				ft_printf("%s", opt.chc->s);
			else
				ft_printf(" %s", opt.chc->s);
			first = 1;
		}
		opt.chc = opt.chc->onext;
	}
	sl_chcdelall(&tmp);
	exit(0);
}

t_chc	*sl_chcnret(t_chc *chc, int add, int sens)
{
	int		i;
	t_chc	*tmp;

	i = 0;
	tmp = chc;
	while (i < add && chc)
	{
		if (sens < 0)
			chc = chc->oprev;
		else
			chc = chc->onext;
		i++;
	}
	if (!chc)
	{
		if (sens < 0)
			return (sl_chcnret(tmp->first->prev, (sl_chclen(tmp->first) % add)
				- tmp->i - 1, -1));
		else
			return (sl_chcnret(tmp->first, tmp->i % add, 1));
	}
	return (chc);
}

void	sl_changecurs(t_chc *des, t_chc *act)
{
	if (!des || !act)
		return ;
	des->curs = 0;
	act->curs = 1;
}

t_chc	*sl_action(t_chc *chc, int line, int act)
{
	t_chc	*curs;

	curs = sl_chccurs(chc);
	if (act == 1)
		sl_changecurs(curs, sl_chcnret(curs, line, -1));
	else if (act == 2)
		sl_changecurs(curs, curs->prev);
	else if (act == 3)
		sl_changecurs(curs, curs->next);
	else if (act == 4)
		sl_changecurs(curs, sl_chcnret(curs, line, 1));
	else if (act == 5)
	{
		curs->select = ((curs->select) ? 0 : 1);
		chc = sl_action(chc, line, 3);
	}
	else if (act == 6)
		sl_changecurs(curs, curs->first);
	else if (act == 7)
		sl_changecurs(curs, curs->first->prev);
	return (chc);
}
