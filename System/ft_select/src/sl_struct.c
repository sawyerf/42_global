/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sl_stuct.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/18 14:42:18 by apeyret           #+#    #+#             */
/*   Updated: 2019/03/04 15:16:30 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

t_chc	*sl_chcnew(char *str)
{
	t_chc	*chc;

	if (!(chc = malloc(sizeof(t_chc))))
		return (NULL);
	if (!(chc->s = ft_strdup(str)))
		return (NULL);
	chc->select = 0;
	chc->curs = 0;
	chc->src = 0;
	chc->i = 0;
	chc->len = ft_strlen(str);
	chc->prev = NULL;
	chc->next = NULL;
	chc->oprev = NULL;
	chc->onext = NULL;
	chc->first = NULL;
	return (chc);
}

int		sl_chclen(t_chc *chc)
{
	int len;

	len = 0;
	while (chc)
	{
		len++;
		chc = chc->onext;
	}
	return (len);
}

t_chc	*sl_chcend(t_chc *chc)
{
	t_chc	*end;

	if (!chc)
		return (NULL);
	end = chc;
	while (end->onext)
	{
		end = end->onext;
	}
	return (end);
}

t_chc	*sl_chcadd(t_chc *chc, t_chc *add)
{
	t_chc	*end;

	if (!chc)
	{
		add->prev = add;
		add->next = add;
		add->first = add;
		add->curs = 1;
		return (add);
	}
	else
	{
		end = sl_chcend(chc);
		end->next = add;
		end->onext = add;
		add->prev = end;
		add->next = chc;
		add->oprev = end;
		add->i = end->i + 1;
		chc->prev = add;
		add->first = chc;
		return (chc);
	}
}

t_chc	*sl_chcadds(t_chc *chc, char *s)
{
	t_chc	*add;

	if (!(add = sl_chcnew(s)))
		return (chc);
	return (sl_chcadd(chc, add));
}
