/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sl_key.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/29 19:04:33 by apeyret           #+#    #+#             */
/*   Updated: 2019/01/29 19:04:48 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void	sl_selekey(t_opt *opt, char *s, int line)
{
	if (!ft_strcmp(K_SPC, s))
		opt->chc = sl_action(opt->chc, line, 5);
	if (!ft_strcmp(K_CTRA, s))
		sl_selectall(opt->chc, 1);
	if (!ft_strcmp(K_CTRD, s))
		sl_selectall(opt->chc, 0);
	if (!ft_strcmp(K_DEL, s) || !ft_strcmp(K_BSPC, s) || !ft_strcmp("d", s))
	{
		opt->chc = sl_chcdel(sl_chccurs(opt->chc));
		opt->len = sl_chclen(opt->chc);
		opt->lmax = sl_lenmax(opt->chc);
		sl_getline(opt);
	}
}

void	sl_exitkey(t_opt *opt, char *s)
{
	if (!ft_strcmp(K_ENTR, s) || !ft_strcmp("w", s))
		sl_return(*opt);
	if (!ft_strcmp(K_ESC, s) || !ft_strcmp("q", s))
		sl_exit(*opt);
}

void	sl_movekey(t_opt *opt, char *s, int line)
{
	if (!ft_strcmp(K_UP, s) || !ft_strcmp("k", s))
		opt->chc = sl_action(opt->chc, line, 2);
	if (!ft_strcmp(K_DWN, s) || !ft_strcmp("j", s))
		opt->chc = sl_action(opt->chc, line, 3);
	if (!ft_strcmp(K_LEFT, s) || !ft_strcmp("h", s))
		opt->chc = sl_action(opt->chc, line, 1);
	if (!ft_strcmp(K_RGHT, s) || !ft_strcmp("l", s))
		opt->chc = sl_action(opt->chc, line, 4);
	if (!ft_strcmp("/", s))
		sl_search(opt);
	if (!ft_strcmp("G", s))
		opt->chc = sl_action(opt->chc, line, 6);
	if (!ft_strcmp("g", s))
		opt->chc = sl_action(opt->chc, line, 7);
}

t_chc	*sl_key(t_opt *opt, char *s, int line)
{
	if (!opt->line)
		return (opt->chc);
	sl_movekey(opt, s, line);
	sl_exitkey(opt, s);
	sl_selekey(opt, s, line);
	ft_printf("\n");
	return (opt->chc);
}
