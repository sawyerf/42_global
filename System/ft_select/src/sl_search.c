/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sl_search.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/23 19:48:55 by apeyret           #+#    #+#             */
/*   Updated: 2019/03/04 15:05:02 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void	sl_slsrc(t_chc *chc, int sel)
{
	while (chc)
	{
		if (chc->src)
		{
			chc->select = sel;
			chc->src = 0;
		}
		chc = chc->onext;
	}
}

t_chc	*sl_chcfsrc(t_chc *chc)
{
	while (chc)
	{
		if (chc->src)
			return (chc);
		chc = chc->onext;
	}
	return (NULL);
}

void	sl_chcchc(t_chc *chc, t_src src)
{
	while (chc)
	{
		if (ft_strstr(chc->s, src.str) && src.str[0])
			chc->src = 1;
		else
			chc->src = 0;
		chc = chc->onext;
	}
}

int		sl_srckey(char *rd, char ret, t_opt *opt)
{
	if (!opt->line)
		return (0);
	else if (ret == 1 && ft_isprint(rd[0]))
		sl_srcadd(&opt->src, rd[0]);
	else if (!ft_strcmp(K_ENTR, rd))
		return (1);
	else if (!ft_strcmp(K_BSPC, rd))
		sl_srcdel(&opt->src);
	else if (!ft_strcmp(K_CTRA, rd))
		sl_slsrc(opt->chc, 1);
	else if (!ft_strcmp(K_CTRD, rd))
		sl_slsrc(opt->chc, 0);
	else if (!ft_strcmp(K_ESC, rd))
		sl_exit(*opt);
	if (!ft_strcmp(K_CTRA, rd) || !ft_strcmp(K_CTRD, rd))
		return (1);
	return (0);
}

void	sl_search(t_opt *opt)
{
	t_chc	*curs;
	char	rd[10];
	int		ret;

	if (sl_srcinit(&opt->src))
		return ;
	curs = sl_chccurs(opt->chc);
	curs->curs = 0;
	while (42)
	{
		sl_chcchc(opt->chc, opt->src);
		sl_putchcall(*opt);
		if ((ret = read(0, rd, 10)) < 0)
			break ;
		rd[ret] = '\0';
		if (sl_srckey(rd, ret, opt))
			break ;
		rd[0] = 0;
	}
	ft_strdel(&opt->src.str);
	if (sl_chcfsrc(opt->chc))
		curs = sl_chcfsrc(opt->chc);
	curs->curs = 1;
}
