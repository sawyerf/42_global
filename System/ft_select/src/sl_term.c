/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sl_term.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/22 20:53:59 by apeyret           #+#    #+#             */
/*   Updated: 2019/03/04 15:59:36 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

void	sl_clr(void)
{
	char	*s;

	if (!(s = tgetstr("cl", NULL)))
		return ;
	tputs(s, 1, &sl_tputchar);
}

int		sl_tputchar(int c)
{
	int fd;

	fd = sl_getfd();
	write(fd, &c, 1);
	close(fd);
	return (1);
}

void	sl_tgpstr(char *s)
{
	char	*tmp;

	if (!(tmp = tgetstr(s, NULL)))
		return ;
	tputs(tmp, 1, &sl_tputchar);
}

int		sl_terminit(struct termios *rest)
{
	char			*tname;
	struct termios	term;

	if (!(tname = getenv("TERM")))
		ft_exit("TERM not set", 1);
	tgetent(NULL, tname);
	if (rest)
		if (tcgetattr(0, rest) == -1)
			return (0);
	if (tcgetattr(0, &term) == -1)
		return (0);
	term.c_lflag &= ~(ICANON);
	term.c_lflag &= ~(ECHO);
	term.c_cc[VMIN] = 1;
	term.c_cc[VTIME] = 0;
	if (tcsetattr(0, TCSADRAIN, &term) == -1)
		return (0);
	sl_tgpstr("vi");
	sl_tgpstr("ti");
	return (1);
}

void	sl_termreset(t_opt opt)
{
	sl_tgpstr("vs");
	sl_tgpstr("te");
	tcsetattr(0, TCSADRAIN, &(opt.term));
}
