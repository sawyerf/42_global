/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/16 17:27:32 by apeyret           #+#    #+#             */
/*   Updated: 2019/02/22 17:38:45 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_select.h"

t_opt	g_opt;

int		sl_lenmax(t_chc *chc)
{
	int	max;

	max = 0;
	while (chc)
	{
		if (max < chc->len)
			max = chc->len;
		chc = chc->onext;
	}
	return (max);
}

void	sl_optinit(t_opt *opt, char **av)
{
	opt->chc = sl_chcinit(av);
	opt->len = sl_chclen(opt->chc);
	opt->lmax = sl_lenmax(opt->chc);
	opt->fd = sl_getfd();
	sl_getline(opt);
	opt->src.str = NULL;
}

int		main(int ac, char **av)
{
	char			rd[11];
	int				ret;

	if (ac < 2)
		ft_exit("usage: ./ft_select [choice ...]", 1);
	sl_optinit(&g_opt, av);
	if (!(sl_terminit(&(g_opt.term))))
		return (1);
	sl_sigset();
	while (42)
	{
		sl_putchcall(g_opt);
		if ((ret = read(0, rd, 10)) < 0)
			break ;
		rd[ret] = '\0';
		if (!(g_opt.chc = sl_key(&g_opt, rd, g_opt.line)))
			break ;
		rd[0] = 0;
	}
	sl_exit(g_opt);
}
