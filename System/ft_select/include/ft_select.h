/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_select.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/16 17:10:33 by apeyret           #+#    #+#             */
/*   Updated: 2019/03/04 15:17:17 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_SELECT_H
# define FT_SELECT_H

# include "libft.h"
# include <curses.h>
# include <termios.h>
# include <term.h>
# include <stdlib.h>
# include <sys/ioctl.h>
# include <fcntl.h>

# define K_UP	"\33[A"
# define K_LEFT	"\33[D"
# define K_RGHT	"\33[C"
# define K_DWN	"\33[B"
# define K_SPC	" "
# define K_ENTR	"\012"
# define K_BSPC "\177"
# define K_DEL	"\33[3~"
# define K_ESC	"\33"
# define K_CTRA	"\1"
# define K_CTRD	"\4"

typedef struct		s_src
{
	char			*str;
	int				size;
	int				allo;
}					t_src;

typedef struct		s_chc
{
	char			*s;
	int				select;
	int				curs;
	int				src;
	int				len;
	int				i;
	struct s_chc	*prev;
	struct s_chc	*next;
	struct s_chc	*onext;
	struct s_chc	*oprev;
	struct s_chc	*first;
}					t_chc;

typedef	struct		s_opt
{
	int				fd;

	t_chc			*chc;
	t_src			src;
	struct termios	term;

	int				line;
	int				col;

	int				len;
	int				lmax;

}					t_opt;

t_chc				*sl_chcinit(char **av);
t_chc				*sl_chcdel(t_chc *chc);
t_chc				*sl_chcnext(t_chc *chc);
t_chc				*sl_chccurs(t_chc *chc);
t_chc				*sl_chcfsrc(t_chc *chc);
void				sl_chcdelall(t_chc **chc);
int					sl_chclen(t_chc *chc);
t_chc				*sl_chcadds(t_chc *chc, char *s);

int					sl_terminit(struct termios *rest);
void				sl_termreset(t_opt opt);

void				sl_sigset();
void				sl_sigquit(int sig);

int					sl_getfd();
void				sl_getline(t_opt *opt);
int					sl_getfirst(t_opt opt);
int					sl_lenmax(t_chc *chc);

void				sl_clr(void);
int					sl_tputchar(int c);

void				sl_search(t_opt	*opt);
void				sl_putchcall(t_opt opt);

t_chc				*sl_key(t_opt *opt, char *s, int max);
t_chc				*sl_action(t_chc *chc, int line, int act);
void				sl_selectall(t_chc *chc, int sl);
void				sl_return(t_opt opt);
void				sl_exit(t_opt opt);
void				sl_selectall(t_chc *chc, int sl);
int					sl_srcinit(t_src *src);
void				sl_srcadd(t_src *src, char c);
void				sl_srcdel(t_src *src);
#endif
