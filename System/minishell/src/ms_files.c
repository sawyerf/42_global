/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ms_files.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 20:02:55 by apeyret           #+#    #+#             */
/*   Updated: 2019/01/15 13:57:29 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		ms_exaccess(char *file)
{
	struct stat st;

	if (access(file, F_OK))
		return (-2);
	stat(file, &st);
	if (S_ISDIR(st.st_mode))
		return (-3);
	if (!access(file, X_OK))
		return (0);
	return (-1);
}

int		ms_foaccess(char *file)
{
	if (access(file, F_OK))
		return (-1);
	return (0);
}
