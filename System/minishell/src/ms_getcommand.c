/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ms_getcommand.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/07 20:44:45 by apeyret           #+#    #+#             */
/*   Updated: 2019/01/15 17:41:19 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

extern		int g_exit;

static char	*ft_strnjoin(char **s1, char *s2, int n)
{
	char	*str;
	int		len;

	if (!s1 && !s2)
		return (NULL);
	len = ft_strlen(*s1);
	if (!(str = ft_strnew(len + n)))
		return (NULL);
	str = ft_strcpy(str, *s1);
	str = ft_strncat(str, s2, n);
	ft_strdel(s1);
	return (str);
}

int			fget_next_line(const int fd, char **line)
{
	static char tmp[BUFF_SIZE + 1];
	char		buf[1];
	int			curs;

	curs = -1;
	*line = NULL;
	if (fd < 0 || read(fd, buf, 0) == -1 || !line)
		return (-1);
	while (42)
	{
		(g_exit) ? ft_strdel(line) : 0;
		if (g_exit)
			return ((tmp[0]) ? -2 : 0);
		(tmp[0]) ? *line = ft_strnjoin(line, tmp, ft_strichr(tmp, '\n', 0)) : 0;
		if (ft_strchr(tmp, '\n'))
		{
			ft_strcpy(tmp, &(tmp[ft_strichr(tmp, '\n', 1)]));
			return (1);
		}
		curs = read(fd, tmp, 30);
		tmp[curs] = '\0';
		if (!curs && !*line)
			return (0);
	}
	return (0);
}

int			ms_checknline(char *command)
{
	int count;

	count = 0;
	while (command[count])
	{
		if (command[count] == '\\' && !command[count + 1])
			return (1);
		else if (command[count] == '\\')
			count++;
		else if (command[count] == '"')
		{
			count++;
			while (command[count] != '"' && command[count])
			{
				if (!ft_strncmp(&command[count], "\\\"", 2))
					count++;
				count++;
			}
			if (!command[count])
				return (2);
		}
		count++;
	}
	return (0);
}

int			ms_gnl(char **line, char **command, int ret)
{
	int reg;

	if ((reg = fget_next_line(0, line)) != 1)
	{
		if (ret <= 1 && reg != -2)
			return (0);
		if (reg == -2)
		{
			ft_strdel(command);
			return (1);
		}
		if (ret == 2)
			ft_dprintf(2,
				"msh: unexpected EOF while looking for matching `\"'\n");
		return (1);
	}
	return (2);
}

int			ms_getcommand(char **command)
{
	char	*line;
	char	*tmp;
	int		ret;
	int		reg;

	ret = 0;
	while (42)
	{
		if ((reg = ms_gnl(&line, command, ret)) != 2)
			return (reg);
		tmp = *command;
		if (!ret)
			*command = ft_zprintf("%s", line);
		else if (ret == 1)
			*command = ft_zprintf("%.*s %s", ft_strlen(*command) - 1, *command,
				line);
		else if (ret == 2)
			*command = ft_zprintf("%s\n%s", *command, line);
		ft_strdel(&tmp);
		ft_strdel(&line);
		if (!*command || !(ret = ms_checknline(*command)))
			return (1);
		ft_printf("> ");
	}
}
