/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ms_command.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 20:02:01 by apeyret           #+#    #+#             */
/*   Updated: 2019/01/30 13:13:48 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char	*ms_where(char *exec, char **path)
{
	int		count;
	int		acc;
	char	*cpath;

	count = 0;
	if (exec[0] == '/' || !ft_strncmp(exec, "./", 2))
	{
		if (!(acc = ms_exaccess(exec)))
			return (ft_strdup(exec));
		(acc == -1) ? ft_dprintf(2, "msh: permission denied: %s\n", exec) : 0;
		(acc == -2) ?
			ft_dprintf(2, "msh: no such file or directory: %s\n", exec) : 0;
		(acc == -3) ? ft_dprintf(2, "msh: %s: is a directory\n", exec) : 0;
		return (NULL);
	}
	while (path[count])
	{
		cpath = ft_zprintf("%s/%s", path[count], exec);
		if (!(acc = ms_exaccess(cpath)))
			return (cpath);
		ft_strdel(&cpath);
		count++;
	}
	ft_printf("msh: command not found: %s\n", exec);
	return (NULL);
}

int		ms_execute(t_comm comm, char **env, char **path)
{
	pid_t	pid;
	int		status;
	char	*exec;

	status = 0;
	if (!(exec = ms_where(comm.exec, path)))
		return (127);
	ms_unsetsig();
	pid = fork();
	if (!pid)
	{
		execve(exec, comm.opt, env);
		exit(0);
	}
	else if (pid < 0)
		ft_printf("[ERROR] nike\n");
	else
		waitpid(pid, &status, 0);
	ft_strdel(&exec);
	if (status)
		return (status / 256);
	return (0);
}

int		ms_command(t_ast *ast, t_gvar *gvar, char ***path)
{
	if (!ast->comm.exec)
		return (gvar->status);
	if (!ft_strcmp(ast->comm.exec, "exit"))
		return (ms_exit(ast, *gvar, path));
	else if (!ft_strcmp(ast->comm.exec, "cd"))
		return (ms_cd(ast->comm.opt[1], &(gvar->env)));
	else if (!ft_strcmp(ast->comm.exec, "echo"))
		return (ms_echo(ast->comm.opt));
	else if (!ft_strcmp(ast->comm.exec, "env"))
		return (ms_env(ast->comm.opt, gvar->env));
	else if (!ft_strcmp(ast->comm.exec, "setenv"))
		return (ms_setenv(ast->comm.opt + 1, &(gvar->env)));
	else if (!ft_strcmp(ast->comm.exec, "unsetenv"))
		return (ms_unsetenv(ast->comm.opt + 1, &(gvar->env)));
	else
		return (-1);
}
