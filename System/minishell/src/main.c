/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/17 13:41:56 by apeyret           #+#    #+#             */
/*   Updated: 2019/01/14 19:06:48 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		g_exit = 0;

void	ms_prompt(int status)
{
	if (!status)
		ft_printf("\x1B[36m(%3d)> \x1B[0m", status);
	else
		ft_printf("\x1B[31m(%3d)> \x1B[0m", status);
}

t_gvar	ms_router(t_ast *ast, t_gvar gvar)
{
	char	**path;

	while (ast)
	{
		path = ms_path(gvar.env);
		if (!ast->type)
		{
			if ((gvar.status = ms_command(ast, &gvar, &path)) == -1)
				gvar.status = ms_execute(ast->comm, gvar.env, path);
		}
		ft_tabdel(&path);
		ast = ast->next;
	}
	return (gvar);
}

void	ms_execo(char **command, t_gvar *gvar)
{
	t_ast	*ast;

	ast = ms_parser(ms_lexer(*command, gvar));
	ft_strdel(command);
	*gvar = ms_router(ast, *gvar);
	ms_astdel(ast);
}

int		main(const int ac, const char **av, const char **env)
{
	char	*command;
	t_gvar	gvar;

	(void)av[ac];
	gvar.env = ms_shlv(ft_tabdup((char**)env));
	gvar.status = 0;
	command = NULL;
	while (42)
	{
		ms_setsig();
		if (g_exit)
			gvar.status = 1;
		else
			ms_prompt(gvar.status);
		g_exit = 0;
		if (!(ms_getcommand(&command)))
			break ;
		ms_execo(&command, &gvar);
	}
	ft_strdel(&command);
	ft_tabdel(&(gvar.env));
	ft_printf("\nexit\n");
	exit(gvar.status);
}
