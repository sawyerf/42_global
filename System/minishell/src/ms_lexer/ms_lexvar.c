/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ms_lexvar.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/10 21:51:08 by apeyret           #+#    #+#             */
/*   Updated: 2019/01/10 21:51:31 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		ms_lexvarlen(char **c)
{
	int count;

	count = 0;
	if (ft_isalpha((*c)[count]) || ft_isdigit((*c)[count]) ||
		ft_cisin("_", (*c)[count]))
	{
		while (ft_isalpha((*c)[count]) || ft_isdigit((*c)[count]) ||
				ft_cisin("_", (*c)[count]))
			count++;
	}
	else
	{
		if (!ft_cisin("?", *c[count]))
		{
			(*c)--;
			return (-1);
		}
		count++;
	}
	return (count);
}

t_lex	*ms_lexvar(t_lex *lex, int type, char **c, t_gvar *gvar)
{
	char	*var;
	char	*in;
	int		count;

	(void)type;
	(*c)++;
	if ((count = ms_lexvarlen(c)) == -1)
		return (ms_lexaddc(lex, type, c, gvar));
	var = ft_zprintf("%.*s=", count, *c);
	*c = *c + count - 1;
	in = ms_dgvarchr(*gvar, var);
	ft_strdel(&var);
	var = in;
	if (in)
	{
		while (*in)
		{
			lex = ms_lexaddc(lex, type, &in, gvar);
			in++;
		}
	}
	ft_strdel(&var);
	return (lex);
}
