/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lexer.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 20:47:16 by apeyret           #+#    #+#             */
/*   Updated: 2019/01/10 23:27:17 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ms_putlex(t_lex *lex)
{
	while (lex)
	{
		ft_printf("%d: %s\n", lex->type, lex->s);
		lex = lex->next;
	}
}

int		ms_type(char c)
{
	if (ft_cisin(";", c))
		return (1);
	return (0);
}

t_lex	*ms_lexer(char *command, t_gvar *gvar)
{
	t_lex	*lex;
	t_cor	cor;

	lex = NULL;
	if (!command)
		return (NULL);
	lex = ms_lexcreat(lex, 0, 0, gvar);
	while (*command)
	{
		cor = ms_corchr(*command);
		lex = cor.f(lex, ms_type(*command), &command, gvar);
		command++;
	}
	lex = ms_lexdelast(lex);
	return (lex);
}
