/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ms_lexdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/10 21:42:00 by apeyret           #+#    #+#             */
/*   Updated: 2019/01/10 23:26:29 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_lex	*ms_lexdelast(t_lex *lex)
{
	t_lex	*tmp;
	t_lex	*ntmp;

	tmp = lex;
	ntmp = lex;
	if (!lex || !lex->s)
	{
		ms_lexdel(lex);
		return (NULL);
	}
	while (tmp->next)
	{
		ntmp = tmp;
		tmp = tmp->next;
	}
	if (tmp && !tmp->s)
	{
		ms_lexdel(tmp);
		if (ntmp)
			ntmp->next = NULL;
	}
	return (lex);
}

void	ms_lexdel(t_lex *lex)
{
	t_lex *tmp;

	while (lex)
	{
		tmp = lex->next;
		ft_strdel(&(lex->s));
		free(lex);
		lex = tmp;
	}
}
