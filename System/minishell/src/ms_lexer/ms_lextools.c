/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ms_lextools.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/10 21:51:38 by apeyret           #+#    #+#             */
/*   Updated: 2019/01/10 21:52:15 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_cor	g_tab[] = {{' ', &ms_lexcreat},
	{'\t', &ms_lexcreat},
	{'\r', &ms_lexcreat},
	{'\v', &ms_lexcreat},
	{'\r', &ms_lexcreat},
	{'\f', &ms_lexcreat},
	{'$', &ms_lexvar},
	{'"', &ms_lexdquote},
	{'\\', &ms_lexbslash},
	{'~', &ms_lextild},
	{';', &ms_lexscolon},
	{'\0', &ms_lexaddc}};

t_cor	ms_corchr(char c)
{
	int	count;

	count = 0;
	while (g_tab[count].c)
	{
		if (g_tab[count].c == c)
			return (g_tab[count]);
		count++;
	}
	return (g_tab[count]);
}

t_lex	*ms_lexend(t_lex *lex)
{
	if (!lex)
		return (lex);
	while (lex->next)
		lex = lex->next;
	return (lex);
}
