/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ms_lexbasic.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/10 21:35:05 by apeyret           #+#    #+#             */
/*   Updated: 2019/01/10 23:26:11 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_lex	*ms_lexadd(t_lex *lex, t_lex *end)
{
	t_lex	*tmp;

	tmp = lex;
	if (!tmp)
		return (end);
	while (tmp->next)
		tmp = tmp->next;
	tmp->next = end;
	return (lex);
}

t_lex	*ms_lexcreat(t_lex *lex, int type, char **c, t_gvar *gvar)
{
	t_lex	*end;
	t_lex	*tmp;

	end = ms_lexend(lex);
	(void)c;
	(void)gvar;
	if (end && !end->s)
	{
		end->type = type;
		return (lex);
	}
	if (!(tmp = malloc(sizeof(t_lex))))
		return (NULL);
	tmp->s = NULL;
	tmp->type = type;
	tmp->len = 0;
	tmp->next = NULL;
	tmp->prev = end;
	lex = ms_lexadd(lex, tmp);
	return (lex);
}

t_lex	*ms_lexcreadd(t_lex *lex, int type, char **c, t_gvar *gvar)
{
	lex = ms_lexcreat(lex, type, c, gvar);
	lex = ms_lexaddc(lex, type, c, gvar);
	return (lex);
}

t_lex	*ms_lexaddc(t_lex *lex, int type, char **c, t_gvar *gvar)
{
	char	*s;
	t_lex	*end;

	end = ms_lexend(lex);
	(void)lex;
	(void)type;
	(void)gvar;
	if (end->len % 128 == 0)
	{
		if (!(s = ft_strnew(end->len + 128)))
			return (NULL);
		ft_strcpy(s, end->s);
		ft_strdel(&(end->s));
		end->s = s;
	}
	end->s[end->len] = **c;
	end->len++;
	end->s[end->len] = '\0';
	return (lex);
}

t_lex	*ms_lexaddoone(t_lex *lex, int type, char **c, t_gvar *gvar)
{
	lex = ms_lexcreadd(lex, type, c, gvar);
	lex = ms_lexcreat(lex, 0, c, gvar);
	return (lex);
}
