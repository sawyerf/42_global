/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ms_lexscara.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/10 21:48:55 by apeyret           #+#    #+#             */
/*   Updated: 2019/01/10 21:50:09 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_lex	*ms_lexscolon(t_lex *lex, int type, char **c, t_gvar *gvar)
{
	t_lex	*end;

	end = ms_lexend(lex);
	if (!end || (!end->s && end->prev && end->prev->type == 1)
		|| (lex && !lex->s) || !ft_strncmp(*c, ";;", 2))
	{
		if (!ft_strncmp(*c, ";;", 2))
			ft_dprintf(2, "msh: syntax error near unexpected token `;;'\n");
		else
			ft_dprintf(2, "msh: syntax error near unexpected token `;'\n");
		gvar->status = 258;
		ms_lexdel(lex);
		while ((*c)[1])
			(*c)++;
		return (NULL);
	}
	return (ms_lexaddoone(lex, type, c, gvar));
}

t_lex	*ms_lexdquote(t_lex *lex, int type, char **c, t_gvar *gvar)
{
	(*c)++;
	while (**c)
	{
		if (**c == '"')
			return (lex);
		else if (!ft_strncmp(*c, "\\\"", 2) || !ft_strncmp(*c, "\\$", 2))
		{
			(*c)++;
			lex = ms_lexaddc(lex, type, c, gvar);
		}
		else if (**c == '$')
			lex = ms_lexvar(lex, type, c, gvar);
		else
			lex = ms_lexaddc(lex, type, c, gvar);
		(*c)++;
	}
	(*c)--;
	ft_dprintf(2, "msh: syntax error: unexpected end of file\n");
	ms_lexdel(lex);
	return (NULL);
}

t_lex	*ms_lexbslash(t_lex *lex, int type, char **c, t_gvar *gvar)
{
	(*c)++;
	if (**c)
		lex = ms_lexaddc(lex, type, c, gvar);
	else
		(**c)--;
	return (lex);
}

t_lex	*ms_lextild(t_lex *lex, int type, char **c, t_gvar *gvar)
{
	char	*home;

	(void)c;
	if (!(home = ms_varchr(gvar->env, "HOME=")))
		return (lex);
	while (*home)
	{
		lex = ms_lexaddc(lex, type, &home, gvar);
		home++;
	}
	return (lex);
}
