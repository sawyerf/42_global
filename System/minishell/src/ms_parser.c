/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ms_lexer.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/03 20:22:19 by apeyret           #+#    #+#             */
/*   Updated: 2019/01/10 23:19:33 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		ms_lexarglen(t_lex *lex)
{
	int count;

	count = 0;
	while (lex && !lex->type)
	{
		count++;
		lex = lex->next;
	}
	return (count);
}

t_ast	*ms_addcomm(t_ast *ast, t_lex **lex)
{
	t_ast	*add;
	int		count;

	count = 0;
	if (!(add = ms_astcreat()))
		return (NULL);
	add->comm.exec = ft_strdup((*lex)->s);
	if (!(add->comm.opt = ft_tabnew(ms_lexarglen(*lex))))
		return (ast);
	while (*lex && !(*lex)->type)
	{
		add->comm.opt[count] = ft_strdup((*lex)->s);
		count++;
		*lex = (*lex)->next;
	}
	return (ms_astadd(ast, add));
}

t_ast	*ms_parser(t_lex *lex)
{
	t_lex	*tmp;
	t_ast	*ast;

	ast = NULL;
	tmp = lex;
	while (tmp)
	{
		if (!tmp->type)
			ast = ms_addcomm(ast, &tmp);
		if (tmp)
			tmp = tmp->next;
	}
	ms_lexdel(lex);
	return (ast);
}
