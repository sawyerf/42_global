/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ms_env.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 20:03:50 by apeyret           #+#    #+#             */
/*   Updated: 2019/01/15 16:08:45 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		ms_varcheck(char *var)
{
	int count;

	count = 0;
	if (!var)
		return (0);
	while (var[count])
	{
		if (!ft_isalpha(var[count]) && !ft_isdigit(var[count])
				&& var[count] != '_')
			return (0);
		count++;
	}
	return (1);
}

int		ms_varcheckequ(char *var)
{
	int count;

	if (!var)
		return (0);
	count = 0;
	while (var[count] != '=' && var[count])
	{
		if (!ft_isalpha(var[count]) && !ft_isdigit(var[count])
				&& var[count] != '_')
			return (0);
		count++;
	}
	return (1);
}

char	*ms_varchr(char **env, char *var)
{
	int		count;

	if (!env || !var)
		return (NULL);
	count = 0;
	while (env[count])
	{
		if (!ft_strncmp(env[count], var, ft_strlen(var)))
			return (env[count] + ft_strlen(var));
		count++;
	}
	return (NULL);
}

char	*ms_dgvarchr(t_gvar gvar, char *var)
{
	int		count;

	if (!gvar.env || !var)
		return (NULL);
	count = 0;
	if (!ft_strcmp(var, "?="))
		return (ft_itoa(gvar.status));
	while (gvar.env[count])
	{
		if (!ft_strncmp(gvar.env[count], var, ft_strlen(var)))
			return (ft_strdup(gvar.env[count] + ft_strlen(var)));
		count++;
	}
	return (NULL);
}

char	**ms_path(char **env)
{
	char	*path;
	char	**tab;

	if (!(path = ft_strdup(ms_varchr(env, "PATH="))))
		path = ft_strdup("./");
	tab = ft_strsplit(path, ':');
	ft_strdel(&path);
	return (tab);
}
