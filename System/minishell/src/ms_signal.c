/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ms_signal.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/08 15:08:50 by apeyret           #+#    #+#             */
/*   Updated: 2019/01/14 18:27:00 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

extern int	g_exit;

void	ms_noexit(int sig)
{
	if (sig == 3)
		ft_printf("Quit: 3");
	ft_printf("\n");
}

void	ms_dexit(int sig)
{
	(void)sig;
	ft_printf("\n");
	g_exit = 1;
	ms_prompt(1);
}

void	ms_setsig(void)
{
	signal(SIGINT, ms_dexit);
	signal(SIGQUIT, ms_dexit);
}

void	ms_unsetsig(void)
{
	signal(SIGINT, ms_noexit);
	signal(SIGQUIT, ms_noexit);
}
