/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ms_ast.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/04 19:44:36 by apeyret           #+#    #+#             */
/*   Updated: 2019/01/10 22:21:25 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ms_commdel(t_comm comm)
{
	ft_strdel(&(comm.exec));
	ft_tabdel(&(comm.opt));
}

void	ms_astdel(t_ast *ast)
{
	t_ast	*tmp;

	while (ast)
	{
		tmp = ast->next;
		if (!ast->type)
			ms_commdel(ast->comm);
		free(ast);
		ast = NULL;
		ast = tmp;
	}
}

t_ast	*ms_astcreat(void)
{
	t_ast	*ast;

	if (!(ast = malloc(sizeof(t_ast))))
		return (NULL);
	ast->type = 0;
	ast->comm.exec = NULL;
	ast->comm.opt = NULL;
	ast->next = NULL;
	return (ast);
}

t_ast	*ms_astadd(t_ast *ast, t_ast *add)
{
	t_ast	*end;

	if (!ast)
		return (add);
	end = ast;
	while (end->next)
		end = end->next;
	end->next = add;
	return (ast);
}
