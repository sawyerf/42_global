/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/17 16:52:21 by apeyret           #+#    #+#             */
/*   Updated: 2019/01/16 14:20:13 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# include "libft.h"
# include <signal.h>
# include <unistd.h>
# include <sys/wait.h>
# include <sys/stat.h>

typedef struct		s_lex
{
	char			*s;
	int				type;
	int				len;
	struct s_lex	*next;
	struct s_lex	*prev;
}					t_lex;

typedef struct		s_gvar
{
	char			**env;
	int				status;
}					t_gvar;

typedef struct		s_cor
{
	char			c;
	t_lex			*(*f)(t_lex *lex, int type, char **c, t_gvar *gvar);
}					t_cor;

typedef struct		s_comm
{
	char			*exec;
	char			**opt;
}					t_comm;

typedef struct		s_ast
{
	int				type;
	t_comm			comm;
	struct s_ast	*next;
}					t_ast;

void				ms_prompt(int status);

int					ms_execute(t_comm comm, char **env, char **path);
int					ms_command(t_ast *ast, t_gvar *gvar, char ***path);

int					ms_exaccess(char *file);
int					ms_foaccess(char *file);

char				*ms_varchr(char **env, char *var);
char				*ms_dgvarchr(t_gvar gvar, char *var);
int					ms_varcheck(char *var);
int					ms_varcheckequ(char *var);
char				**ms_path(char **env);

t_lex				*ms_lexer(char *command, t_gvar *gvar);
t_lex				*ms_lexdquote(t_lex *lex, int type, char **c, t_gvar *gvar);
t_lex				*ms_lexbslash(t_lex *lex, int type, char **c, t_gvar *gvar);
t_lex				*ms_lexdelast(t_lex *lex);
t_lex				*ms_lextild(t_lex *lex, int type, char **c, t_gvar *gvar);
t_lex				*ms_lexadd(t_lex *lex, t_lex *end);
t_lex				*ms_lexcreat(t_lex *lex, int type, char **c, t_gvar *gvar);
t_lex				*ms_lexcreadd(t_lex *lex, int type, char **c, t_gvar *gvar);
t_lex				*ms_lexaddc(t_lex *lex, int type, char **c, t_gvar *gvar);
t_lex				*ms_lexvar(t_lex *lex, int type, char **c, t_gvar *gvar);
t_lex				*ms_lexaddoone(t_lex *lex, int type, char **c,
					t_gvar *gvar);
t_lex				*ms_lexscolon(t_lex *lex, int type, char **c, t_gvar *gvar);
void				ms_lexdel(t_lex *lex);
t_lex				*ms_lexend(t_lex *lex);

t_cor				ms_corchr(char c);

int					ms_env(char **av, char **env);
int					ms_setenv(char **av, char ***env);
int					ms_unsetenv(char **av, char ***env);
char				**ms_csetenv(char **env, char *var);
char				**ms_envdel(char **env, char *var);
char				**ms_envaddint(char **env, char *var, int value);
char				**ms_envaddstr(char **env, char *var, char *value);
char				*ms_envchrr(char **env, char *var);

char				**ms_shlv(char **env);

t_ast				*ms_parser(t_lex *lex);
t_ast				*ms_astcreat();
t_ast				*ms_astadd(t_ast *ast, t_ast *add);
void				ms_astdel(t_ast *ast);

int					ms_getcommand(char **command);

char				*ms_where(char *exec, char **path);
char				*ms_pathjoin(char *p1, char *p2);
int					ms_cd(char *path, char ***env);
int					ms_echo(char **av);
int					ms_exit(t_ast *ast, t_gvar gvar, char ***path);

void				ms_setsig(void);
void				ms_unsetsig(void);

#endif
