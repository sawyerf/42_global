/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/16 13:58:09 by apeyret           #+#    #+#             */
/*   Updated: 2019/01/16 13:58:13 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	get_value(char c)
{
	if (c >= '0' && c <= '9')
		return (c - '0');
	else if (c >= 'A' && c <= 'F')
		return (c - 'A' + 10);
	return (0);
}

int			ft_atoi_base(char *str, int base)
{
	int	res;
	int	signe;

	res = 0;
	while (*str == 32 || (*str >= 9 && *str <= 13))
		str++;
	signe = (*str == '-') ? -1 : 1;
	(*str == '+' || *str == '-') ? str++ : 0;
	while (ft_ncisin("0123456789ABCDEF", *str, base))
		res = res * base + get_value(*str++);
	return (res * signe);
}
