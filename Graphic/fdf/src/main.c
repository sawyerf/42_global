/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apeyret <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/09 15:01:43 by apeyret           #+#    #+#             */
/*   Updated: 2019/04/09 17:40:14 by apeyret          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mlx.h"
#define LARG 500
#define LONG 500

int main()
{
	void	*mlx;
	void	*win;
	char	*img;

	mlx = mlx_init();
	win = mlx_new_window(mlx, LARG, LONG, "fdf");
	img = mlx_new_image(mlx, LARG, LONG);
	mlx_loop(mlx);
}
